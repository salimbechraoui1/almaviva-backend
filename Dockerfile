FROM openjdk:11
COPY  ./target/spring_crud_app-0.0.1-SNAPSHOT.jar  spring_crud_app-0.0.1-SNAPSHOT.jar
CMD  ["java","-jar","spring_crud_app-0.0.1-SNAPSHOT.jar"]